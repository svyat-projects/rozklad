const app = require('express')()

const visitsServiceConstructor = require("./services/visitsService");
const htmlParsingServerConstructor = require("./services/htmlParsingService");
const rozkladServiceConstructor = require("./services/rozkladService");

const Config = require("./Config");
const redisClient = require('./redisClient/redisClient')
const axios = require('axios')

const visitsService = visitsServiceConstructor({config: Config, redisClient})
const htmlParsingService = htmlParsingServerConstructor()
const rozkladService = rozkladServiceConstructor({
    redis: redisClient, config: Config, htmlParsingService, axios
})

const pretty = json => {
    const withHumanReadable = {...json, fetchedAt: new Date(json.fetchedAt || Date.now())}
    return `<div style="white-space: break-spaces">${JSON.stringify(withHumanReadable, 2, 4)}</div>`
}

rozkladService.initAutoUpdating()

app.get('/visits/:group', async (req, res) => {
    const visits = await visitsService.getVisits(req.params.group)
    res.status(200).send(visits)
})

app.get('/visits', async (req, res) => {
    const visits = await visitsService.getAllVisits()
    res.status(200).send(pretty(visits))
})

app.get('/api/visits', async (req, res) => {
    const visits = await visitsService.getAllVisits()
    res.status(200).json(visits)
})

app.get('/', (req, res) =>
    res.status(200).send(`<h1><a href="https://gitlab.com/svyatoslav.shilkov.2012/rozklad">Documentation</a></h1>`))

app.get('/:groupId', async (req, res) => {
    if (req.params.groupId === 'favicon.ico') {
        res.sendStatus(404)
    } else {
        visitsService.incVisits(req.params.groupId)
        rozkladService.getRozklad(req.params.groupId).then(rozk => res.status(200).send(rozk))
    }
})

app.get('/api/v1/search', async (req, res) => {
    visitsService.incVisits('/api/search')
    if (!req.query.query)
        res.status(400).send(`'query' parameter must exist in query`)
    else
        rozkladService.globalSearch(req.query.query).then(result => res.status(200).json(result))
})

app.get('/api/v1/pretty/search', async (req, res) => {
    visitsService.incVisits('/api/pretty/search')
    if (!req.query.query)
        res.status(400).send(`'query' parameter must exist in query`)
    else
        rozkladService.globalSearch(req.query.query).then(result => res.status(200).send(pretty(result)))
})

app.get('/api/v1/all', async (req, res) => {
    visitsService.incVisits('/api/all')
    rozkladService.getAllRozkladData().then(rozk => res.status(200).json(rozk))
})

app.get('/api/v1/:groupId', async (req, res) => {
    visitsService.incVisits('api/' + req.params.groupId)
    rozkladService.getRozkladData(req.params.groupId).then(rozk => res.status(200).json(rozk))
})

app.get('/api/v1/pretty/:groupId', async (req, res) => {
    visitsService.incVisits('api/pretty/' + req.params.groupId)
    rozkladService.getRozkladData(req.params.groupId).then(rozk => res.status(200).send(pretty(rozk)))
})

app.listen(5001, () => console.log('listening'))
